package com.orcsoft.training.workshop.model;

import java.math.BigDecimal;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class ReqReceivePromptPay {
	private String SendBankCode;
	private String SendAccountID;
	private String ReceiveBankCode;
	private String ReceiveAccountID;
	private BigDecimal Amount;

	public String getSendBankCode() {
		return SendBankCode;
	}

	@JsonProperty("SendBankCode")
	public void setSendBankCode(String sendBankCode) {
		SendBankCode = sendBankCode;
	}

	public String getSendAccountID() {
		return SendAccountID;
	}

	@JsonProperty("SendAccountID")
	public void setSendAccountID(String sendAccountID) {
		SendAccountID = sendAccountID;
	}

	public String getReceiveBankCode() {
		return ReceiveBankCode;
	}

	@JsonProperty("ReceiveBankCode")
	public void setReceiveBankCode(String receiveBankCode) {
		ReceiveBankCode = receiveBankCode;
	}

	public String getReceiveAccountID() {
		return ReceiveAccountID;
	}

	@JsonProperty("ReceiveAccountID")
	public void setReceiveAccountID(String receiveAccountID) {
		ReceiveAccountID = receiveAccountID;
	}

	public BigDecimal getAmount() {
		return Amount;
	}

	@JsonProperty("Amount")
	public void setAmount(BigDecimal amount) {
		Amount = amount;
	}

	@Override
	public String toString() {
		return "ReqReceivePromptPay [SendBankCode=" + SendBankCode + ", SendAccountID=" + SendAccountID
				+ ", ReceiveBankCode=" + ReceiveBankCode + ", ReceiveAccountID=" + ReceiveAccountID + ", Amount="
				+ Amount + "]";
	}

}
