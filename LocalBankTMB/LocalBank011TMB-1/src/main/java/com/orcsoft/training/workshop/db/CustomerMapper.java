package com.orcsoft.training.workshop.db;

import java.util.List;

import org.apache.ibatis.annotations.Select;

public interface CustomerMapper {

	@Select("SELECT CustomerID as CustomerID, Login as Login, Password as Password, Name as Name, PhoneNum as PhoneNum FROM Customer WHERE Login=#{login}")
	public List<CustomerObject> selectLoginCustomerTable(String login);

	@Select("SELECT CustomerID as CustomerID, Login as Login, Password as Password, Name as Name, PhoneNum as PhoneNum FROM Customer WHERE Login=#{cusID}")
	public List<CustomerObject> selectCusIDCustomerTable(Long cusID);
}
