package com.orcsoft.training.workshop.db;

public class AccCusObject {
	private String Name;
	private String StatusCode;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

}
