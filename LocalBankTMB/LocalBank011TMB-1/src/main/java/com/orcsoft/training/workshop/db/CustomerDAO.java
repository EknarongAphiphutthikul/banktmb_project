package com.orcsoft.training.workshop.db;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

public class CustomerDAO {

	public List<CustomerObject> selectLoginCustomerTable(String login) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		CustomerMapper customerMapper = session.getMapper(CustomerMapper.class);
		List<CustomerObject> customer = customerMapper.selectLoginCustomerTable(login);
		session.close();
		return customer;
	}

	public List<CustomerObject> selectCusIDCustomerTable(Long cusID) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		CustomerMapper customerMapper = session.getMapper(CustomerMapper.class);
		List<CustomerObject> customer = customerMapper.selectCusIDCustomerTable(cusID);
		session.close();
		return customer;
	}
}
