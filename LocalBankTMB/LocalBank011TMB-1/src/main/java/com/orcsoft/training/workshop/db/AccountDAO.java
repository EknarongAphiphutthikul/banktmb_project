package com.orcsoft.training.workshop.db;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

public class AccountDAO {

	public AccountObject selectAccIDAccountTable(String accID) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper accountMapper = session.getMapper(AccountMapper.class);
		AccountObject account = accountMapper.selecctAccIDAccountTable(accID);
		session.close();
		return account;
	}

	public List<AccountObject> selectCusIDAccountTable(Long cusID) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper accountMapper = session.getMapper(AccountMapper.class);
		List<AccountObject> account = accountMapper.selecctCusIDAccountTable(cusID);
		session.close();
		return account;
	}

	public boolean updateAccountTable(AccountObject accObj) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper accountMapper = session.getMapper(AccountMapper.class);
		int countRowUpdate = accountMapper.updateAccount(accObj);
		if(countRowUpdate == 1) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
	
	public boolean updateAccountTable(AccountObject accObj1, AccountObject accObj2) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper accountMapper = session.getMapper(AccountMapper.class);
		int countRowUpdate1 = accountMapper.updateAccount(accObj1);
		int countRowUpdate2 = accountMapper.updateAccount(accObj2);
		if((countRowUpdate1 + countRowUpdate2) == 2) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
	
	public boolean updateAccountTable(AccountObject accObj1, AccountObject accObj2, AccountObject accObj3) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccountMapper accountMapper = session.getMapper(AccountMapper.class);
		int countRowUpdate1 = accountMapper.updateAccount(accObj1);
		int countRowUpdate2 = accountMapper.updateAccount(accObj2);
		int countRowUpdate3 = accountMapper.updateAccount(accObj3);
		if((countRowUpdate1 + countRowUpdate2 + countRowUpdate3) == 3) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
}
