package com.orcsoft.training.workshop.controller;

@SuppressWarnings("serial")
public class ExceptionDataNotFound extends Exception {
	public ExceptionDataNotFound(String msg) {
		super(msg);
	}
}
