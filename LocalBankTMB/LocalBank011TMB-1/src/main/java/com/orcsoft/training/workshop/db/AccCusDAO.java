package com.orcsoft.training.workshop.db;

import org.apache.ibatis.session.SqlSession;

public class AccCusDAO {
	
	public AccCusObject selecctOneAccountReceive(String accID) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		AccCusMapper accCusMapper = session.getMapper(AccCusMapper.class);
		AccCusObject accountReceive = accCusMapper.selecctOneAccountReceive(accID);
		session.close();
		return accountReceive;
	}
}
