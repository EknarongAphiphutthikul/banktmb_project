package com.orcsoft.training.workshop.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class PostResAuthentication {
	private String statusAuthentication;
	private Long CustomerID;
	private String Name;

	public PostResAuthentication() {
		super();
	}

	public PostResAuthentication(String statusReq, String statusAuthentication, Long customerID, String name) {
		this.statusAuthentication = statusAuthentication;
		CustomerID = customerID;
		Name = name;
	}

	public String getStatusAuthentication() {
		return statusAuthentication;
	}

	@JsonProperty("statusAuthentication")
	public void setStatusAuthentication(String statusAuthentication) {
		this.statusAuthentication = statusAuthentication;
	}

	public Long getCustomerID() {
		return CustomerID;
	}

	@JsonProperty("CustomerID")
	public void setCustomerID(Long customerID) {
		CustomerID = customerID;
	}

	public String getName() {
		return Name;
	}

	@JsonProperty("Name")
	public void setName(String Name) {
		this.Name = Name;
	}

	@Override
	public String toString() {
		return "PostResAuthentication [statusAuthentication=" + statusAuthentication
				+ ", CustomerID=" + CustomerID + ", Name=" + Name + "]";
	}
}
