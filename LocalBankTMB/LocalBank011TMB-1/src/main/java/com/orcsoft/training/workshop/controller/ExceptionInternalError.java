package com.orcsoft.training.workshop.controller;

@SuppressWarnings("serial")
public class ExceptionInternalError extends Exception {

	public ExceptionInternalError(String msg) {
		super(msg);
	}

}
