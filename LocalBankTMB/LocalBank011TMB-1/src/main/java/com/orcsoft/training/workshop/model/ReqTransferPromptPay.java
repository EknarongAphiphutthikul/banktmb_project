package com.orcsoft.training.workshop.model;

import java.math.BigDecimal;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class ReqTransferPromptPay {
	private String APIID;
	private String SendBankCode;
	private String SendAccountID;
	private BigDecimal Amount;

	public ReqTransferPromptPay(String aPIID, String sendBankCode, String sendAccountID, BigDecimal amount) {
		APIID = aPIID;
		SendBankCode = sendBankCode;
		SendAccountID = sendAccountID;
		Amount = amount;
	}

	public String getAPIID() {
		return APIID;
	}

	@JsonProperty("AIPID")
	public void setAPIID(String aPIID) {
		APIID = aPIID;
	}

	public String getSendBankCode() {
		return SendBankCode;
	}

	@JsonProperty("SendBankCode")
	public void setSendBankCode(String sendBankCode) {
		SendBankCode = sendBankCode;
	}

	public String getSendAccountID() {
		return SendAccountID;
	}

	@JsonProperty("SendAccountID")
	public void setSendAccountID(String sendAccountID) {
		SendAccountID = sendAccountID;
	}

	public BigDecimal getAmount() {
		return Amount;
	}

	@JsonProperty("Amount")
	public void setAmount(BigDecimal amount) {
		Amount = amount;
	}

	@Override
	public String toString() {
		return "ReqTransferPromptPay [APIID=" + APIID + ", SendBankCode=" + SendBankCode + ", SendAccountID="
				+ SendAccountID + ", Amount=" + Amount + "]";
	}

}
