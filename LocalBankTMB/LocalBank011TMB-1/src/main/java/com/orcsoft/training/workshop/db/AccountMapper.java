package com.orcsoft.training.workshop.db;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface AccountMapper {

	@Select("SELECT AccountID as AccountID, CustomerID as CustomerID, BankCode as BankCode, AccountType as AccountType, StatusCode as StatusCode, BalanceAmount as BalanceAmount, CreateDTM as CreateDTM, UpdateDTM as UpdateDTM FROM Account WHERE AccountID=#{accID}")
	public AccountObject selecctAccIDAccountTable(String accID);

	@Select("SELECT AccountID as AccountID, CustomerID as CustomerID, BankCode as BankCode, AccountType as AccountType, StatusCode as StatusCode, BalanceAmount as BalanceAmount, CreateDTM as CreateDTM, UpdateDTM as UpdateDTM FROM Account WHERE CustomerID=#{cusID} and StatusCode='ACT'")
	public List<AccountObject> selecctCusIDAccountTable(Long cusID);
	
	@Update("UPDATE Account SET BalanceAmount=#{BalanceAmount}, UpdateDTM=#{UpdateDTM} WHERE AccountID=#{AccountID}")
	public int updateAccount(AccountObject tran);
}
