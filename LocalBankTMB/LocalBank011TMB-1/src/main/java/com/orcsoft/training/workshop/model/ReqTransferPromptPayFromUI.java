package com.orcsoft.training.workshop.model;

import java.math.BigDecimal;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class ReqTransferPromptPayFromUI {
	private BigDecimal SubmitAmount;
	private String SendBankCode;
	private String SendAccountID;
	private String type;
	private String value;

	public BigDecimal getSubmitAmount() {
		return SubmitAmount;
	}

	@JsonProperty("SubmitAmount")
	public void setSubmitAmount(BigDecimal submitAmount) {
		SubmitAmount = submitAmount;
	}

	public String getSendBankCode() {
		return SendBankCode;
	}

	@JsonProperty("SendBankCode")
	public void setSendBankCode(String sendBankCode) {
		SendBankCode = sendBankCode;
	}

	public String getSendAccountID() {
		return SendAccountID;
	}

	@JsonProperty("SendAccountID")
	public void setSendAccountID(String sendAccountID) {
		SendAccountID = sendAccountID;
	}

	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ReqTransferPromptPayFromUI [SubmitAmount=" + SubmitAmount + ", SendBankCode=" + SendBankCode
				+ ", SendAccountID=" + SendAccountID + ", type=" + type + ", value=" + value + "]";
	}
	
}
