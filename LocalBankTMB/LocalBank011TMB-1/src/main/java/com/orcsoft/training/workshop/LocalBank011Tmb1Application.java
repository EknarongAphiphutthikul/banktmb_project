package com.orcsoft.training.workshop;

import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LocalBank011Tmb1Application implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(LocalBank011Tmb1Application.class, args);
	}
	@Value("${server.port}")
	public String serverPort;
	@Value("${server.servlet.contextPath}")
	public String contextPath;
    @Override
    public void run(String... strings) throws Exception {
        System.out.println("Environment HostName => "+InetAddress.getLocalHost().getHostName());
        System.out.println("Environment HostAddress =>"+InetAddress.getLocalHost().getHostAddress()); 
        System.out.println("http://localhost:"+serverPort+contextPath+"/swagger-ui.html");
    }
}
