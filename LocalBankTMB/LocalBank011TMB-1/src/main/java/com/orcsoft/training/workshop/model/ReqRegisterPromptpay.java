package com.orcsoft.training.workshop.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class ReqRegisterPromptpay {
	private String IDType;
	private String IDValue;
	private String BankCode;
	private String AccountID;
	private String AccountName;

	public String getIDType() {
		return IDType;
	}

	@JsonProperty("IDType")
	public void setIDType(String iDType) {
		IDType = iDType;
	}

	public String getIDValue() {
		return IDValue;
	}

	@JsonProperty("IDValue")
	public void setIDValue(String iDValue) {
		IDValue = iDValue;
	}

	public String getBankCode() {
		return BankCode;
	}

	@JsonProperty("BankCode")
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getAccountID() {
		return AccountID;
	}

	@JsonProperty("AccountID")
	public void setAccountID(String accountID) {
		AccountID = accountID;
	}

	public String getAccountName() {
		return AccountName;
	}

	@JsonProperty("AccountName")
	public void setAccountName(String accountName) {
		AccountName = accountName;
	}

	@Override
	public String toString() {
		return "register_Info [IDType=" + IDType + ", IDValue=" + IDValue + ", BankCode=" + BankCode + ", AccountID="
				+ AccountID + ", AccountName=" + AccountName + "]";
	}

}
