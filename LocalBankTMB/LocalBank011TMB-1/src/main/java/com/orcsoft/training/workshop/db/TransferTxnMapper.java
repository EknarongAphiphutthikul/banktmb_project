package com.orcsoft.training.workshop.db;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.orcsoft.training.workshop.model.PostResStatement;
import com.orcsoft.training.workshop.model.ReqStatement;

public interface TransferTxnMapper {

	@Select("SELECT TxnID as TxnID From  TransferTxn WHERE TxnType=#{TxnType} and TxnState=#{TxnState}  and FeeAmount=#{FeeAmount} and SubmitAmount=#{SubmitAmount} and NetAmount=#{NetAmount} and SendBankCode=#{SendBankCode} and SendAccountID=#{SendAccountID}  and ReceiveBankCode=#{ReceiveBankCode} and ReceiveAccountID=#{ReceiveAccountID} and CreateDTM=#{CreateDTM}")
	public TransferTxnObject selectTxnID(TransferTxnObject tran);

	@Select("SELECT * FROM TransferTxn " + 
			"WHERE ((SendAccountID = #{AccountID} AND TxnType='S') OR (ReceiveAccountID =  #{AccountID} AND TxnType='R')) AND TxnState='Complete' AND CreateDTM BETWEEN #{Start} AND #{End} " + 
			"ORDER BY CreateDTM DESC ")
	public List<PostResStatement> selecctByTimeStatement(ReqStatement req);

	@Select("SELECT * FROM TransferTxn " + 
			"WHERE ((SendAccountID = #{AccountID} AND TxnType='S') OR (ReceiveAccountID =  #{AccountID} AND TxnType='R')) AND TxnState='Complete' " + 
			"ORDER BY CreateDTM DESC " + 
			"LIMIT 5;")
	public List<PostResStatement> selecctTopFiveStatement(ReqStatement req);

	@Insert("INSERT INTO TransferTxn(TxnType, TxnState, TxnNote, FeeAmount, SubmitAmount, NetAmount, SendBankCode, SendAccountID, AIPID, ReceiveBankCode, ReceiveAccountID, CreateDTM) "
			+ "VALUES (#{TxnType}, #{TxnState}, #{TxnNote}, #{FeeAmount}, #{SubmitAmount}, #{NetAmount}, #{SendBankCode}, #{SendAccountID}, #{AIPID}, #{ReceiveBankCode}, #{ReceiveAccountID}, #{CreateDTM})")
	public int insertTransfer(TransferTxnObject tran);

	@Update("UPDATE TransferTxn SET TxnState=#{TxnState}, NetAmount=#{NetAmount}, TxnNote=#{TxnNote} WHERE TxnID=#{TxnID}")
	public int updateTransfer(TransferTxnObject tran);

}
