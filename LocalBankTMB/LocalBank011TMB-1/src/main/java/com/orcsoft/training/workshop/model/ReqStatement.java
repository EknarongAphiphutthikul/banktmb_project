package com.orcsoft.training.workshop.model;

import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class ReqStatement {
	private String AccountID;
	private Date Start;
	private Date End;
	private String Type;

	public String getAccountID() {
		return AccountID;
	}

	@JsonProperty("AccountID")
	public void setAccountID(String accountID) {
		AccountID = accountID;
	}

	public Date getStart() {
		return Start;
	}

	@JsonProperty("Start")
	public void setStart(Date start) {
		Start = start;
	}

	public Date getEnd() {
		return End;
	}

	@JsonProperty("End")
	public void setEnd(Date end) {
		End = end;
	}

	public String getType() {
		return Type;
	}

	@JsonProperty("Type")
	public void setType(String type) {
		Type = type;
	}

	@Override
	public String toString() {
		return "ReqStatement [AccountID=" + AccountID + ", Start=" + Start + ", End=" + End + ", Type=" + Type + "]";
	}

}
