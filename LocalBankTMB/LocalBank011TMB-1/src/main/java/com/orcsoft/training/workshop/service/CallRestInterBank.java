package com.orcsoft.training.workshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.orcsoft.training.workshop.controller.ExceptionDataNotFound;
import com.orcsoft.training.workshop.controller.ExceptionInternalError;
import com.orcsoft.training.workshop.model.GetResPromptpayFromInterBank;
import com.orcsoft.training.workshop.model.PostResTransferPromptPay;
import com.orcsoft.training.workshop.model.ReqRegisterPromptpay;
import com.orcsoft.training.workshop.model.ReqTransferPromptPay;

@Service
public class CallRestInterBank {

	@Autowired
	private RestTemplate restTemplate;

	private String hostInterBank = "http://192.168.9.154:8090/interbank";

	public String registerPromptpay(ReqRegisterPromptpay req) {
		try {
			// return
			// restTemplate.postForObject("http://192.168.9.154:8090/interbank/any-id",req,String.class);
			HttpHeaders header = new HttpHeaders();
			header.add(HttpHeaders.CONTENT_TYPE, "application/json");
			HttpEntity<?> postReq = new HttpEntity<Object>(req, header);
			ResponseEntity<String> out = restTemplate.exchange(hostInterBank + "/any-id", HttpMethod.POST, postReq,
					String.class);
			return out.getStatusCode().toString();
		} catch (Exception se) {
			return se.getMessage().toString().split(" ")[0];
		}
	}

	public GetResPromptpayFromInterBank getPromptpay(String type, String value)
			throws ExceptionInternalError, ExceptionDataNotFound {
		try {
			// GetRawPromptpay resp = restTemplate.getForObject("/any-id/?type=" + type +
			// "&value=" + value, GetRawPromptpay.class);
			ResponseEntity<GetResPromptpayFromInterBank> out = restTemplate.exchange(
					hostInterBank + "/any-id/?type=" + type + "&value=" + value, HttpMethod.GET, null,
					GetResPromptpayFromInterBank.class);
			return out.getBody();
		} catch (Exception se) {
			String temp = se.getMessage().toString().split(" ")[0];
			if (temp.equals("404")) {
				throw new ExceptionDataNotFound("Account Not Found");
			} else {
				throw new ExceptionInternalError(se.getMessage());
			}
		}
	}

	public String deletePromptpay(String type, String value) throws ExceptionInternalError, ExceptionDataNotFound {
		int id = getPromptpay(type, value).getAIPID();
		try {
			ResponseEntity<String> out = restTemplate.exchange(hostInterBank + "/any-id/" + id, HttpMethod.DELETE, null,
					String.class);
			return out.getStatusCode().toString();
		} catch (Exception se) {
			throw new ExceptionInternalError(se.getMessage());
		}
	}

	public PostResTransferPromptPay transferPromptPay(ReqTransferPromptPay req) throws ExceptionInternalError {
		try {
			HttpHeaders header = new HttpHeaders();
			header.add(HttpHeaders.CONTENT_TYPE, "application/json");
			HttpEntity<?> postReq = new HttpEntity<Object>(req, header);
			ResponseEntity<PostResTransferPromptPay> out = restTemplate.exchange(hostInterBank + "/money-tranfer",
					HttpMethod.POST, postReq, PostResTransferPromptPay.class);
			return out.getBody();
		} catch (Exception se) {
			throw new ExceptionInternalError(se.getMessage());
		}
	}
}
