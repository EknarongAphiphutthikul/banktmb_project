package com.orcsoft.training.workshop.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.orcsoft.training.workshop.model.ExceptionRespon;

@ControllerAdvice
public class ExceptionHandlerBankTMB {

	@ExceptionHandler(value = { ExceptionInternalError.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	ExceptionRespon handleInternalErrorException(ExceptionInternalError ex) throws Exception {
		return new ExceptionRespon(500, ex.getMessage());
	}

	@ExceptionHandler(value = { ExceptionDataNotFound.class })
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	ExceptionRespon handleNotFoundException(ExceptionDataNotFound ex) throws Exception {
		return new ExceptionRespon(404, ex.getMessage());
	}
}
