package com.orcsoft.training.workshop.db;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.orcsoft.training.workshop.model.PostResStatement;
import com.orcsoft.training.workshop.model.ReqStatement;

public class TransferTxnDAO {

	public TransferTxnObject selectTxnID(TransferTxnObject tran) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		TransferTxnObject transfer = transferMapper.selectTxnID(tran);
		session.close();
		return transfer;
	}

	public List<PostResStatement> selecctByTimeStatement(ReqStatement req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		List<PostResStatement> transfer = transferMapper.selecctByTimeStatement(req);
		session.close();
		return transfer;
	}

	public List<PostResStatement> selecctTopFiveStatement(ReqStatement req) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		List<PostResStatement> transfer = transferMapper.selecctTopFiveStatement(req);
		session.close();
		return transfer;
	}

	public boolean insertTransfer(TransferTxnObject tran) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		int transfer = transferMapper.insertTransfer(tran);
		if (transfer == 1) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
	
	public boolean insertTransfer(TransferTxnObject tran1, TransferTxnObject tran2) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		int transfer1 = transferMapper.insertTransfer(tran1);
		int transfer2 = transferMapper.insertTransfer(tran2);
		if ((transfer1 + transfer2) == 2) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
	
	public boolean insertTransfer(TransferTxnObject tran1, TransferTxnObject tran2, TransferTxnObject tran3) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		int transfer1 = transferMapper.insertTransfer(tran1);
		int transfer2 = transferMapper.insertTransfer(tran2);
		int transfer3 = transferMapper.insertTransfer(tran3);
		if ((transfer1 + transfer2 + transfer3) == 3) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}

	public boolean updateTransfer(TransferTxnObject tran) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		int transfer = transferMapper.updateTransfer(tran);
		if (transfer == 1) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
	
	public boolean updateTransfer(TransferTxnObject tran1, TransferTxnObject tran2) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		int transfer1 = transferMapper.updateTransfer(tran1);
		int transfer2 = transferMapper.updateTransfer(tran2);
		if ((transfer1 + transfer2) == 2) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
	
	public boolean updateTransfer(TransferTxnObject tran1, TransferTxnObject tran2, TransferTxnObject tran3) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();
		TransferTxnMapper transferMapper = session.getMapper(TransferTxnMapper.class);
		int transfer1 = transferMapper.updateTransfer(tran1);
		int transfer2 = transferMapper.updateTransfer(tran2);
		int transfer3 = transferMapper.updateTransfer(tran3);
		if ((transfer1 + transfer2 + transfer3) == 3) {
			session.commit();
			session.close();
			return true;
		}
		session.rollback();
		session.close();
		return false;
	}
}
