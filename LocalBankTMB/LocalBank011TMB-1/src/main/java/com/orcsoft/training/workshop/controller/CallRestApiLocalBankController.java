package com.orcsoft.training.workshop.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orcsoft.training.workshop.db.AccCusDAO;
import com.orcsoft.training.workshop.db.AccCusObject;
import com.orcsoft.training.workshop.db.AccountDAO;
import com.orcsoft.training.workshop.db.AccountObject;
import com.orcsoft.training.workshop.db.CustomerDAO;
import com.orcsoft.training.workshop.db.CustomerObject;
import com.orcsoft.training.workshop.db.TransferTxnDAO;
import com.orcsoft.training.workshop.db.TransferTxnObject;
import com.orcsoft.training.workshop.model.GetResAccountTable;
import com.orcsoft.training.workshop.model.GetResDataAccountReceive;
import com.orcsoft.training.workshop.model.PostResAuthentication;
import com.orcsoft.training.workshop.model.PostResStatement;
import com.orcsoft.training.workshop.model.PostResTransfer;
import com.orcsoft.training.workshop.model.ReqAuthentication;
import com.orcsoft.training.workshop.model.ReqStatement;
import com.orcsoft.training.workshop.model.ReqTransferTMB;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/v1/callRestApiLocalBank")
public class CallRestApiLocalBankController {

	@PostMapping(value = "/authentication", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PostResAuthentication> postAuthentication(HttpServletRequest request,
			@RequestBody ReqAuthentication req) throws ExceptionInternalError {
		try {
			CustomerDAO customerDAO = new CustomerDAO();
			List<CustomerObject> listCustomerObject = customerDAO.selectLoginCustomerTable(req.getUsername());
			PostResAuthentication resAuth = new PostResAuthentication();
			if (listCustomerObject.size() != 0) {
				if (listCustomerObject.get(0).getPassword().equals(req.getPassword())) {
					resAuth.setStatusAuthentication("PasswordCorrect");
					resAuth.setCustomerID(listCustomerObject.get(0).getCustomerID());
					resAuth.setName(listCustomerObject.get(0).getName());
				} else {
					resAuth.setStatusAuthentication("PasswordInCorrect");
				}
			} else {
				resAuth.setStatusAuthentication("UsernameInCorrect");
			}
			return new ResponseEntity<PostResAuthentication>(resAuth, HttpStatus.OK);
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

	@GetMapping(value = "/getAccounts/{cusID}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<GetResAccountTable> getAccounts(HttpServletRequest request, @PathVariable("cusID") Long cusID)
			throws ExceptionInternalError {
		try {
			AccountDAO accountDAO = new AccountDAO();
			List<AccountObject> ListAccountObject = accountDAO.selectCusIDAccountTable(cusID);
			List<GetResAccountTable> listReturn = new ArrayList<GetResAccountTable>();
			for (AccountObject account : ListAccountObject) {
				GetResAccountTable rawResult = new GetResAccountTable();
				rawResult.setAccountID(account.getAccountID());
				rawResult.setCustomerID(account.getCustomerID());
				rawResult.setBankCode(account.getBankCode());
				rawResult.setAccountType(account.getAccountType());
				rawResult.setStatusCode(account.getStatusCode());
				rawResult.setBalanceAmount(account.getBalanceAmount());
				listReturn.add(rawResult);
			}
			return listReturn;
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

	@GetMapping(value = "/getAccountReceive/{accID}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GetResDataAccountReceive> getAccountReceive(HttpServletRequest request,
			@PathVariable("accID") String accID) throws ExceptionInternalError, ExceptionDataNotFound {
		try {
			AccCusDAO accCusDAO = new AccCusDAO();
			AccCusObject accountReceiveObject = accCusDAO.selecctOneAccountReceive(accID);
			GetResDataAccountReceive resAccountReceive = new GetResDataAccountReceive();
			resAccountReceive.setName(accountReceiveObject.getName());
			resAccountReceive.setStatusCode(accountReceiveObject.getStatusCode());
			return new ResponseEntity<GetResDataAccountReceive>(resAccountReceive, HttpStatus.OK);
		} catch (NullPointerException e) {
			throw new ExceptionDataNotFound("Data Not Found");
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

	@PostMapping(value = "/transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PostResTransfer> postTransferTMB(HttpServletRequest request, @RequestBody ReqTransferTMB req)
			throws ExceptionInternalError {
		try {
			final String accountTMB = "011-1-000000-1";
			final String bankCodeTMB = "011";
			boolean checkFreeAmountNotZero = false;
			boolean checkInsertTransfer;
			boolean checkUpdateAccount;
			boolean checkUpdateTransfer;
			TransferTxnObject transObjFreeAmount = new TransferTxnObject();
			if (req.getSubmitAmount().compareTo(new BigDecimal(0)) != 1) {
				return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "SubmitAmountZero"),
						HttpStatus.OK);
			}
			TransferTxnDAO tranferDAO = new TransferTxnDAO();
			AccountDAO accountDAO = new AccountDAO();
			AccountObject accObjectSend = accountDAO.selectAccIDAccountTable(req.getSendAccountID());
			AccountObject accObjectReceive = accountDAO.selectAccIDAccountTable(req.getReceiveAccountID());
			if (accObjectReceive == null) {
				return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "AccountReceiveNotFound"),
						HttpStatus.OK);
			}
			if (!accObjectReceive.getStatusCode().equals("ACT")) {
				return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "AccountReceiveNotActive"),
						HttpStatus.OK);
			}
			if (accObjectSend.getBalanceAmount().compareTo(req.getFeeAmount().add(req.getSubmitAmount())) == -1) {
				return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "NotEnoughMoney"), HttpStatus.OK);
			}

			if (req.getFeeAmount().compareTo(new BigDecimal(0)) == 1) {
				checkFreeAmountNotZero = true;
			}

			Date date = new Date();
			TransferTxnObject transObjSend = new TransferTxnObject();
			transObjSend.setTxnID(null);
			transObjSend.setTxnType("S");
			transObjSend.setTxnState("Process");
			transObjSend.setTxnNote(req.getTxnNote());
			transObjSend.setFeeAmount(req.getFeeAmount());
			transObjSend.setSubmitAmount(req.getSubmitAmount());
			transObjSend.setNetAmount(req.getFeeAmount().add(req.getSubmitAmount()));
			transObjSend.setSendBankCode(req.getSendBankCode());
			transObjSend.setSendAccountID(req.getSendAccountID());
			transObjSend.setAIPID(null);
			transObjSend.setReceiveBankCode(req.getReceiveBankCode());
			transObjSend.setReceiveAccountID(req.getReceiveAccountID());
			transObjSend.setCreateDTM(date);

			TransferTxnObject transObjReceive = new TransferTxnObject();
			transObjReceive.setTxnID(null);
			transObjReceive.setTxnType("R");
			transObjReceive.setTxnState("Process");
			transObjReceive.setTxnNote(null);
			transObjReceive.setFeeAmount(new BigDecimal(0));
			transObjReceive.setSubmitAmount(req.getSubmitAmount());
			transObjReceive.setNetAmount(req.getSubmitAmount());
			transObjReceive.setSendBankCode(req.getSendBankCode());
			transObjReceive.setSendAccountID(req.getSendAccountID());
			transObjReceive.setAIPID(null);
			transObjReceive.setReceiveBankCode(req.getReceiveBankCode());
			transObjReceive.setReceiveAccountID(req.getReceiveAccountID());
			transObjReceive.setCreateDTM(date);

			if (checkFreeAmountNotZero) {
				transObjFreeAmount.setTxnID(null);
				transObjFreeAmount.setTxnType("R");
				transObjFreeAmount.setTxnState("Process");
				transObjFreeAmount.setTxnNote(null);
				transObjFreeAmount.setFeeAmount(new BigDecimal(0));
				transObjFreeAmount.setSubmitAmount(req.getFeeAmount());
				transObjFreeAmount.setNetAmount(req.getFeeAmount());
				transObjFreeAmount.setSendBankCode(req.getSendBankCode());
				transObjFreeAmount.setSendAccountID(req.getSendAccountID());
				transObjFreeAmount.setAIPID(null);
				transObjFreeAmount.setReceiveBankCode(bankCodeTMB);
				transObjFreeAmount.setReceiveAccountID(accountTMB);
				transObjFreeAmount.setCreateDTM(date);
				checkInsertTransfer = tranferDAO.insertTransfer(transObjSend, transObjReceive, transObjFreeAmount);
			} else {

				checkInsertTransfer = tranferDAO.insertTransfer(transObjSend, transObjReceive);
			}
			if (checkInsertTransfer) {
				Long TxnIDSend = tranferDAO.selectTxnID(transObjSend).getTxnID();
				Long TxnIDReceive = tranferDAO.selectTxnID(transObjReceive).getTxnID();
				transObjSend.setTxnID(TxnIDSend);
				transObjReceive.setTxnID(TxnIDReceive);
				accObjectSend.setBalanceAmount(
						accObjectSend.getBalanceAmount().subtract(req.getFeeAmount().add(req.getSubmitAmount())));
				accObjectSend.setUpdateDTM(date);
				accObjectReceive.setBalanceAmount(accObjectReceive.getBalanceAmount().add(req.getSubmitAmount()));
				accObjectReceive.setUpdateDTM(date);
				if (checkFreeAmountNotZero) {
					Long TxnIDFreeAmount = tranferDAO.selectTxnID(transObjFreeAmount).getTxnID();
					transObjFreeAmount.setTxnID(TxnIDFreeAmount);
					transObjFreeAmount.setTxnNote(TxnIDSend.toString());
					AccountObject accObjectTMB = accountDAO.selectAccIDAccountTable(accountTMB);
					accObjectTMB.setBalanceAmount(accObjectTMB.getBalanceAmount().add(req.getFeeAmount()));
					accObjectTMB.setUpdateDTM(date);
					checkUpdateAccount = accountDAO.updateAccountTable(accObjectSend, accObjectReceive, accObjectTMB);
				} else {
					checkUpdateAccount = accountDAO.updateAccountTable(accObjectSend, accObjectReceive);
				}
				if (checkUpdateAccount) {
					transObjSend.setTxnState("Complete");
					transObjReceive.setTxnState("Complete");
					if (checkFreeAmountNotZero) {
						transObjFreeAmount.setTxnState("Complete");
						checkUpdateTransfer = tranferDAO.updateTransfer(transObjSend, transObjReceive,
								transObjFreeAmount);
					} else {
						checkUpdateTransfer = tranferDAO.updateTransfer(transObjSend, transObjReceive);
					}
					System.out.println(checkUpdateTransfer);
					return new ResponseEntity<PostResTransfer>(new PostResTransfer(true), HttpStatus.OK);
				} else {
					transObjSend.setTxnState("Fail");
					transObjSend.setNetAmount(new BigDecimal(0));
					transObjReceive.setTxnState("Fail");
					transObjReceive.setNetAmount(new BigDecimal(0));
					if (checkFreeAmountNotZero) {
						transObjFreeAmount.setTxnState("Fail");
						transObjFreeAmount.setNetAmount(new BigDecimal(0));
						checkUpdateTransfer = tranferDAO.updateTransfer(transObjSend, transObjReceive,
								transObjFreeAmount);
					} else {
						checkUpdateTransfer = tranferDAO.updateTransfer(transObjSend, transObjReceive);
					}
					System.out.println(checkUpdateTransfer);
					return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "UpdateAccountNotComplete"),
							HttpStatus.OK);
				}
			}
			return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "InsertTranNotComplete"),
					HttpStatus.OK);
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

	@PostMapping(value = "/showStatementTopFive", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<PostResStatement> getStatementTopFive(HttpServletRequest request, @RequestBody ReqStatement req)
			throws ExceptionInternalError {
		try {
			TransferTxnDAO tranferDAO = new TransferTxnDAO();
			List<PostResStatement> statementObject = tranferDAO.selecctTopFiveStatement(req);
			return statementObject;
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

	@PostMapping(value = "/showStatementByTime", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public List<PostResStatement> getStatementMyTime(HttpServletRequest request, @RequestBody ReqStatement req)
			throws ExceptionInternalError {
		try {
			TransferTxnDAO tranferDAO = new TransferTxnDAO();
			List<PostResStatement> statementObject = tranferDAO.selecctByTimeStatement(req);
			return statementObject;
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

}
