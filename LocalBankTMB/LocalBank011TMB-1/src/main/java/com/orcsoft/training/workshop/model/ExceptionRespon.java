package com.orcsoft.training.workshop.model;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class ExceptionRespon {
	private int statusCode;
	private String msg;

	public ExceptionRespon(int statusCode, String msg) {
		super();
		this.statusCode = statusCode;
		this.msg = msg;
	}

	public int getStatusCode() {
		return statusCode;
	}

	@JsonProperty("statusCode")
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMsg() {
		return msg;
	}

	@JsonProperty("msg")
	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "ExceptionRespon [statusCode=" + statusCode + ", msg=" + msg + "]";
	}
}
