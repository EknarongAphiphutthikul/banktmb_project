package com.orcsoft.training.workshop.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.couchbase.client.deps.com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class GetResDataAccountReceive {
	private String Name;
	private String StatusCode;

	public String getName() {
		return Name;
	}

	@JsonProperty("Name")
	public void setName(String name) {
		Name = name;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	@JsonProperty("StatusCode")
	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	@Override
	public String toString() {
		return "GetResDataAccountReceive [Name=" + Name + ", StatusCode=" + StatusCode + "]";
	}

}
