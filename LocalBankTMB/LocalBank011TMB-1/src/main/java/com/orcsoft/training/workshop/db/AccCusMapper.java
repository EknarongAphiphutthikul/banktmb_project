package com.orcsoft.training.workshop.db;

import org.apache.ibatis.annotations.Select;

public interface AccCusMapper {
	
	@Select("SELECT Name as Name, StatusCode as StatusCode FROM Account INNER JOIN Customer ON Customer.CustomerID = Account.CustomerID WHERE AccountID=#{accID}")
	public AccCusObject selecctOneAccountReceive(String accID);
}
