package com.orcsoft.training.workshop.model;

import java.math.BigDecimal;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class ReqTransferTMB {
	private BigDecimal SubmitAmount;
	private BigDecimal FeeAmount;
	private String SendBankCode;
	private String SendAccountID;
	private String ReceiveBankCode;
	private String ReceiveAccountID;
	private String TxnNote;

	public BigDecimal getSubmitAmount() {
		return SubmitAmount;
	}
	
	@JsonProperty("SubmitAmount")
	public void setSubmitAmount(BigDecimal submitAmount) {
		SubmitAmount = submitAmount;
	}

	public BigDecimal getFeeAmount() {
		return FeeAmount;
	}

	@JsonProperty("FeeAmount")
	public void setFeeAmount(BigDecimal feeAmount) {
		FeeAmount = feeAmount;
	}

	public String getSendBankCode() {
		return SendBankCode;
	}

	@JsonProperty("SendBankCode")
	public void setSendBankCode(String sendBankCode) {
		SendBankCode = sendBankCode;
	}

	public String getSendAccountID() {
		return SendAccountID;
	}

	@JsonProperty("SendAccountID")
	public void setSendAccountID(String sendAccountID) {
		SendAccountID = sendAccountID;
	}

	public String getReceiveBankCode() {
		return ReceiveBankCode;
	}

	@JsonProperty("ReceiveBankCode")
	public void setReceiveBankCode(String receiveBankCode) {
		ReceiveBankCode = receiveBankCode;
	}

	public String getReceiveAccountID() {
		return ReceiveAccountID;
	}

	@JsonProperty("ReceiveAccountID")
	public void setReceiveAccountID(String receiveAccountID) {
		ReceiveAccountID = receiveAccountID;
	}

	public String getTxnNote() {
		return TxnNote;
	}

	@JsonProperty("TxnNote")
	public void setTxnNote(String txnNote) {
		TxnNote = txnNote;
	}

	@Override
	public String toString() {
		return "ReqTransferTMB [SubmitAmount=" + SubmitAmount + ", FeeAmount=" + FeeAmount + ", SendBankCode=" + SendBankCode
				+ ", SendAccountID=" + SendAccountID + ", ReceiveBankCode=" + ReceiveBankCode + ", ReceiveAccountID="
				+ ReceiveAccountID + ", TxnNote=" + TxnNote + "]";
	}

}
