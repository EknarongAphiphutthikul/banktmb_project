package com.orcsoft.training.workshop.controller;

import java.math.BigDecimal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orcsoft.training.workshop.db.AccountDAO;
import com.orcsoft.training.workshop.db.AccountObject;
import com.orcsoft.training.workshop.db.TransferTxnDAO;
import com.orcsoft.training.workshop.db.TransferTxnObject;
import com.orcsoft.training.workshop.model.AnyResPromptPayToUI;
import com.orcsoft.training.workshop.model.GetResPromptpayFromInterBank;
import com.orcsoft.training.workshop.model.PostResReceiveTransferPromptPay;
import com.orcsoft.training.workshop.model.PostResTransfer;
import com.orcsoft.training.workshop.model.PostResTransferPromptPay;
import com.orcsoft.training.workshop.model.ReqReceivePromptPay;
import com.orcsoft.training.workshop.model.ReqRegisterPromptpay;
import com.orcsoft.training.workshop.model.ReqTransferPromptPay;
import com.orcsoft.training.workshop.model.ReqTransferPromptPayFromUI;
import com.orcsoft.training.workshop.service.CallRestInterBank;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/v1/callRestApiInterBank")
public class CallRestApiInterBankController {
	@Autowired
	CallRestInterBank callRestServiceInterBank;

	@PostMapping(value = "/registerPromptpay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AnyResPromptPayToUI> postRegisterPromptpayJson(HttpServletRequest request,
			@RequestBody ReqRegisterPromptpay req) throws ExceptionInternalError {
		try {
			String status_code = callRestServiceInterBank.registerPromptpay(req);
			AnyResPromptPayToUI res;
			if (status_code.equals("200"))
				res = new AnyResPromptPayToUI(200, "success");
			else if (status_code.equals("400"))
				res = new AnyResPromptPayToUI(400, "exists");
			else if (status_code.equals("500"))
				res = new AnyResPromptPayToUI(500, "incorrectFormat");
			else
				res = new AnyResPromptPayToUI(999, "anyError");
			return new ResponseEntity<AnyResPromptPayToUI>(res, HttpStatus.OK);
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

	@GetMapping(value = "/getPromptpay/{type}/{value}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public GetResPromptpayFromInterBank getPromptpay(HttpServletRequest request, @PathVariable("type") String type,
			@PathVariable("value") String value) throws ExceptionInternalError, ExceptionDataNotFound {
		return callRestServiceInterBank.getPromptpay(type, value);
	}

	@DeleteMapping(value = "/deletePromptpay/{type}/{value}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AnyResPromptPayToUI> deletePromptpay(HttpServletRequest request,
			@PathVariable("type") String type, @PathVariable("value") String value)
			throws ExceptionInternalError, ExceptionDataNotFound {
		String status = callRestServiceInterBank.deletePromptpay(type, value);
		return new ResponseEntity<AnyResPromptPayToUI>(new AnyResPromptPayToUI(Integer.parseInt(status), "success"),
				HttpStatus.OK);
	}

	@PostMapping(value = "/TransferPromptpay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PostResTransfer> postTransferPromptpay(HttpServletRequest request,
			@RequestBody ReqTransferPromptPayFromUI req) throws ExceptionInternalError, ExceptionDataNotFound {
		try {
			if (req.getSubmitAmount().compareTo(new BigDecimal(0)) != 1) {
				return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "SubmitAmountZero"),
						HttpStatus.OK);
			}
			GetResPromptpayFromInterBank dataReceive = callRestServiceInterBank.getPromptpay(req.getType(),
					req.getValue());
			if (!dataReceive.getStatus().equals("Active")) {
				return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "AccountReceiveNotActive"),
						HttpStatus.OK);
			}
			TransferTxnDAO tranferDAO = new TransferTxnDAO();
			AccountDAO accountDAO = new AccountDAO();
			AccountObject accObjectSend = accountDAO.selectAccIDAccountTable(req.getSendAccountID());
			if (accObjectSend.getBalanceAmount().compareTo(req.getSubmitAmount()) == -1) {
				return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "NotEnoughMoney"), HttpStatus.OK);
			}
			Date date = new Date();
			TransferTxnObject transObjSend = new TransferTxnObject();
			transObjSend.setTxnID(null);
			transObjSend.setTxnType("S");
			transObjSend.setTxnState("Process");
			transObjSend.setTxnNote(null);
			transObjSend.setFeeAmount(new BigDecimal(0));
			transObjSend.setSubmitAmount(req.getSubmitAmount());
			transObjSend.setNetAmount(req.getSubmitAmount());
			transObjSend.setSendBankCode(req.getSendBankCode());
			transObjSend.setSendAccountID(req.getSendAccountID());
			transObjSend.setAIPID(new Long(dataReceive.getAIPID()));
			transObjSend.setReceiveBankCode(dataReceive.getBankCode());
			transObjSend.setReceiveAccountID(dataReceive.getAccountID());
			transObjSend.setCreateDTM(date);
			boolean checkInsertTransferSent = tranferDAO.insertTransfer(transObjSend);
			if (checkInsertTransferSent) {
				Long TxnIDSend = tranferDAO.selectTxnID(transObjSend).getTxnID();
				transObjSend.setTxnID(TxnIDSend);
				accObjectSend.setBalanceAmount(accObjectSend.getBalanceAmount().subtract(req.getSubmitAmount()));
				accObjectSend.setUpdateDTM(date);
				boolean checkRowUpdateAccountSent = accountDAO.updateAccountTable(accObjectSend);
				if (checkRowUpdateAccountSent) {
					ReqTransferPromptPay reqToInterBank = new ReqTransferPromptPay(
							Integer.toString(dataReceive.getAIPID()), req.getSendBankCode(), req.getSendAccountID(),
							req.getSubmitAmount());
					PostResTransferPromptPay resFromInterBank = callRestServiceInterBank
							.transferPromptPay(reqToInterBank);
					System.out.println(resFromInterBank);
					transObjSend.setTxnState("Complete");
					transObjSend.setTxnNote(resFromInterBank.getTxnRefID().toString());
					boolean checkRowUpdateTransferSent = tranferDAO.updateTransfer(transObjSend);
					System.out.println(checkRowUpdateTransferSent);
					return new ResponseEntity<PostResTransfer>(new PostResTransfer(true), HttpStatus.OK);
				} else {
					transObjSend.setTxnState("Fail");
					transObjSend.setNetAmount(new BigDecimal(0));
					boolean checkRowUpdateTransferSent = tranferDAO.updateTransfer(transObjSend);
					System.out.println(checkRowUpdateTransferSent);
					return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "UpdateAccountNotComplete"),
							HttpStatus.OK);
				}
			}
			return new ResponseEntity<PostResTransfer>(new PostResTransfer(false, "InsertTranNotComplete"),
					HttpStatus.OK);
		} catch (NullPointerException e) {
			throw new ExceptionDataNotFound("Data Not Found");
		} catch (Exception e) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}

	@PostMapping(value = "/ReceiveTransferPromptpay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<PostResReceiveTransferPromptPay> postReceiveTransferPromptpay(HttpServletRequest request,
			@RequestBody ReqReceivePromptPay req) throws ExceptionInternalError, ExceptionDataNotFound {
		try {
			System.out.println("Reveice Action!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.out.println(req);
			Date date = new Date();
			AccountDAO accountDAO = new AccountDAO();
			AccountObject accObjectReceive = accountDAO.selectAccIDAccountTable(req.getReceiveAccountID());
			accObjectReceive.setBalanceAmount(accObjectReceive.getBalanceAmount().add(req.getAmount()));
			accObjectReceive.setUpdateDTM(date);
			TransferTxnDAO tranferDAO = new TransferTxnDAO();
			TransferTxnObject transObjReceive = new TransferTxnObject();
			transObjReceive.setTxnID(null);
			transObjReceive.setTxnType("R");
			transObjReceive.setTxnNote(null);
			transObjReceive.setFeeAmount(new BigDecimal(0));
			transObjReceive.setSubmitAmount(req.getAmount());
			transObjReceive.setSendBankCode(req.getSendBankCode());
			transObjReceive.setSendAccountID(req.getSendAccountID());
			transObjReceive.setAIPID(null);
			transObjReceive.setReceiveBankCode(req.getReceiveBankCode());
			transObjReceive.setReceiveAccountID(req.getReceiveAccountID());
			transObjReceive.setCreateDTM(date);
			PostResReceiveTransferPromptPay resReceiveTransferPromptPay = new PostResReceiveTransferPromptPay();
			resReceiveTransferPromptPay.setAmount(req.getAmount());
			resReceiveTransferPromptPay.setTxnDTM(date);
			boolean checkRowUpdateAccountReceive = accountDAO.updateAccountTable(accObjectReceive);
			if (checkRowUpdateAccountReceive) {
				transObjReceive.setTxnState("Complete");
				transObjReceive.setNetAmount(req.getAmount());
				resReceiveTransferPromptPay.setResult("Success");
			} else {
				transObjReceive.setTxnState("Fail");
				transObjReceive.setNetAmount(new BigDecimal(0));
				resReceiveTransferPromptPay.setResult("Fail");
			}
			boolean checkInsertTransferReceive = tranferDAO.insertTransfer(transObjReceive);
			System.out.println(checkInsertTransferReceive);
			Long TxnIDReceive = tranferDAO.selectTxnID(transObjReceive).getTxnID();
			resReceiveTransferPromptPay.setTxnRefID(TxnIDReceive);
			return new ResponseEntity<PostResReceiveTransferPromptPay>(resReceiveTransferPromptPay, HttpStatus.OK);
		} catch (NullPointerException ex) {
			throw new ExceptionDataNotFound("Account Not Found");
		} catch (Exception ex) {
			throw new ExceptionInternalError("Internal Server Error");
		}
	}
}
