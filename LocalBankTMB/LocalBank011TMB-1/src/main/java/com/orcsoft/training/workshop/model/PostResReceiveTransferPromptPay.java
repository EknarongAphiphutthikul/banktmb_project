package com.orcsoft.training.workshop.model;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class PostResReceiveTransferPromptPay {
	private Long TxnRefID;
	private Date TxnDTM;
	private BigDecimal Amount;
	private String Result;

	public Long getTxnRefID() {
		return TxnRefID;
	}
	
	@JsonProperty("TxnRefID")
	public void setTxnRefID(Long txnRefID) {
		TxnRefID = txnRefID;
	}

	public Date getTxnDTM() {
		return TxnDTM;
	}

	@JsonProperty("TxnDTM")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public void setTxnDTM(Date txnDTM) {
		TxnDTM = txnDTM;
	}

	public BigDecimal getAmount() {
		return Amount;
	}

	@JsonProperty("Amount")
	public void setAmount(BigDecimal amount) {
		Amount = amount;
	}

	public String getResult() {
		return Result;
	}

	@JsonProperty("Result")
	public void setResult(String result) {
		Result = result;
	}

	@Override
	public String toString() {
		return "PostResReceiveTransferPromptPay [TxnRefID=" + TxnRefID + ", TxnDTM=" + TxnDTM + ", Amount=" + Amount
				+ ", Result=" + Result + "]";
	}

}
