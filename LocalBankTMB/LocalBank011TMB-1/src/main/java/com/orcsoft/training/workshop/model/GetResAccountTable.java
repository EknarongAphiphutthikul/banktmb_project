package com.orcsoft.training.workshop.model;

import java.math.BigDecimal;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class GetResAccountTable {
	private String AccountID;
	private Long CustomerID;
	private String BankCode;
	private String AccountType;
	private String StatusCode;
	private BigDecimal BalanceAmount;

	public String getAccountID() {
		return AccountID;
	}

	@JsonProperty("AccountID")
	public void setAccountID(String accountID) {
		AccountID = accountID;
	}

	public Long getCustomerID() {
		return CustomerID;
	}

	@JsonProperty("CustomerID")
	public void setCustomerID(Long customerID) {
		CustomerID = customerID;
	}

	public String getBankCode() {
		return BankCode;
	}

	@JsonProperty("BankCode")
	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getAccountType() {
		return AccountType;
	}

	@JsonProperty("AccountType")
	public void setAccountType(String accountType) {
		AccountType = accountType;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	@JsonProperty("StatusCode")
	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	public BigDecimal getBalanceAmount() {
		return BalanceAmount;
	}

	@JsonProperty("BalanceAmount")
	public void setBalanceAmount(BigDecimal balanceAmount) {
		BalanceAmount = balanceAmount;
	}

	@Override
	public String toString() {
		return "GetRawAccountTable [AccountID=" + AccountID + ", CustomerID=" + CustomerID + ", BankCode=" + BankCode
				+ ", AccountType=" + AccountType + ", StatusCode=" + StatusCode + ", BalanceAmount=" + BalanceAmount
				+ "]";
	}

}
