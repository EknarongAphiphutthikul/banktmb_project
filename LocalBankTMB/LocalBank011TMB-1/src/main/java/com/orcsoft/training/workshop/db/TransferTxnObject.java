package com.orcsoft.training.workshop.db;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferTxnObject {
	private Long TxnID;
	private String TxnType;
	private String TxnState;
	private String TxnNote;
	private BigDecimal FeeAmount;
	private BigDecimal SubmitAmount;
	private BigDecimal NetAmount;
	private String SendBankCode;
	private String SendAccountID;
	private Long AIPID;
	private String ReceiveBankCode;
	private String ReceiveAccountID;
	private Date CreateDTM;

	public Long getTxnID() {
		return TxnID;
	}

	@JsonProperty("TxnID")
	public void setTxnID(Long txnID) {
		TxnID = txnID;
	}

	public String getTxnType() {
		return TxnType;
	}

	@JsonProperty("TxnType")
	public void setTxnType(String txnType) {
		TxnType = txnType;
	}

	public String getTxnState() {
		return TxnState;
	}

	@JsonProperty("TxnState")
	public void setTxnState(String txnState) {
		TxnState = txnState;
	}

	public String getTxnNote() {
		return TxnNote;
	}

	@JsonProperty("TxnNote")
	public void setTxnNote(String txnNote) {
		TxnNote = txnNote;
	}

	public BigDecimal getFeeAmount() {
		return FeeAmount;
	}

	@JsonProperty("FeeAmount")
	public void setFeeAmount(BigDecimal feeAmount) {
		FeeAmount = feeAmount;
	}

	public BigDecimal getSubmitAmount() {
		return SubmitAmount;
	}

	@JsonProperty("SubmitAmount")
	public void setSubmitAmount(BigDecimal submitAmount) {
		SubmitAmount = submitAmount;
	}

	public BigDecimal getNetAmount() {
		return NetAmount;
	}

	@JsonProperty("NetAmount")
	public void setNetAmount(BigDecimal netAmount) {
		NetAmount = netAmount;
	}

	public String getSendBankCode() {
		return SendBankCode;
	}

	@JsonProperty("SendBankCode")
	public void setSendBankCode(String sendBankCode) {
		SendBankCode = sendBankCode;
	}

	public String getSendAccountID() {
		return SendAccountID;
	}

	@JsonProperty("SendAccountID")
	public void setSendAccountID(String sendAccountID) {
		SendAccountID = sendAccountID;
	}

	public Long getAIPID() {
		return AIPID;
	}

	@JsonProperty("AIPID")
	public void setAIPID(Long aIPID) {
		AIPID = aIPID;
	}

	public String getReceiveBankCode() {
		return ReceiveBankCode;
	}

	@JsonProperty("ReceiveBankCode")
	public void setReceiveBankCode(String receiveBankCode) {
		ReceiveBankCode = receiveBankCode;
	}

	public String getReceiveAccountID() {
		return ReceiveAccountID;
	}

	@JsonProperty("ReceiveAccountID")
	public void setReceiveAccountID(String receiveAccountID) {
		ReceiveAccountID = receiveAccountID;
	}

	public Date getCreateDTM() {
		return CreateDTM;
	}

	@JsonProperty("CreateDTM")
	public void setCreateDTM(Date createDTM) {
		CreateDTM = createDTM;
	}	
}
