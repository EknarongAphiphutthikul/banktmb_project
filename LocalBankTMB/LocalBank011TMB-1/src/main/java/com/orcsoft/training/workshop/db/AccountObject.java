package com.orcsoft.training.workshop.db;

import java.math.BigDecimal;
import java.util.Date;

public class AccountObject {
	private String AccountID;
	private Long CustomerID;
	private String BankCode;
	private String AccountType;
	private String StatusCode;
	private BigDecimal BalanceAmount;
	private Date CreateDTM;
	private Date UpdateDTM;

	public String getAccountID() {
		return AccountID;
	}

	public void setAccountID(String accountID) {
		AccountID = accountID;
	}

	public Long getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(Long customerID) {
		CustomerID = customerID;
	}

	public String getBankCode() {
		return BankCode;
	}

	public void setBankCode(String bankCode) {
		BankCode = bankCode;
	}

	public String getAccountType() {
		return AccountType;
	}

	public void setAccountType(String accountType) {
		AccountType = accountType;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	public BigDecimal getBalanceAmount() {
		return BalanceAmount;
	}

	public void setBalanceAmount(BigDecimal balanceAmount) {
		BalanceAmount = balanceAmount;
	}

	public Date getCreateDTM() {
		return CreateDTM;
	}

	public void setCreateDTM(Date createDTM) {
		CreateDTM = createDTM;
	}

	public Date getUpdateDTM() {
		return UpdateDTM;
	}

	public void setUpdateDTM(Date updateDTM) {
		UpdateDTM = updateDTM;
	}

}
