package com.orcsoft.training.workshop.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class PostResTransferPromptPay {
	private Long TxnRefID;
	private String TxnDTM;

	public Long getTxnRefID() {
		return TxnRefID;
	}

	@JsonProperty("TxnRefID")
	public void setTxnRefID(Long txnRefID) {
		TxnRefID = txnRefID;
	}

	public String getTxnDTM() {
		return TxnDTM;
	}

	@JsonProperty("TxnDTM")
	public void setTxnDTM(String txnDTM) {
		TxnDTM = txnDTM;
	}

	@Override
	public String toString() {
		return "PostResTransferPromptPay [TxnRefID=" + TxnRefID + ", TxnDTM=" + TxnDTM + "]";
	}

}
