package com.orcsoft.training.workshop.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class PostResTransfer {
	private boolean statusTransfer;
	private String msg;
	
	public PostResTransfer(boolean statusTransfer) {
		super();
		this.statusTransfer = statusTransfer;
	}
	
	public PostResTransfer(boolean statusTransfer, String msg) {
		super();
		this.statusTransfer = statusTransfer;
		this.msg = msg;
	}

	public boolean isStatusTransfer() {
		return statusTransfer;
	}

	@JsonProperty("statusTransfer")
	public void setStatusTransfer(boolean statusTransfer) {
		this.statusTransfer = statusTransfer;
	}

	public String getMsg() {
		return msg;
	}
	
	@JsonProperty("msg")
	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "PostResTransfer [statusTransfer=" + statusTransfer + ", msg=" + msg + "]";
	}
}
