package com.orcsoft.training.workshop.model;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class PostResStatement {
	private String TxnType;
	private BigDecimal NetAmount;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date CreateDTM;

	public String getTxnType() {
		return TxnType;
	}

	@JsonProperty("TxnType")
	public void setTxnType(String txnType) {
		TxnType = txnType;
	}

	public BigDecimal getNetAmount() {
		return NetAmount;
	}

	@JsonProperty("NetAmount")
	public void setNetAmount(BigDecimal netAmount) {
		NetAmount = netAmount;
	}

	public Date getCreateDTM() {
		return CreateDTM;
	}

	@JsonProperty("CreateDTM")
	public void setCreateDTM(Date createDTM) {
		CreateDTM = createDTM;
	}

	@Override
	public String toString() {
		return "PostResStatement [TxnType=" + TxnType + ", NetAmount=" + NetAmount + ", CreateDTM=" + CreateDTM + "]";
	}

}
