package com.orcsoft.training.workshop.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
@Scope("prototype")
public class AnyResPromptPayToUI {
	private int StatusCode;
	private String msg;
	
	public AnyResPromptPayToUI(int statusCode, String msg) {
		StatusCode = statusCode;
		this.msg = msg;
	}

	public int getStatusCode() {
		return StatusCode;
	}

	@JsonProperty("StatusCode")
	public void setStatusCode(int statusCode) {
		StatusCode = statusCode;
	}

	public String getMsg() {
		return msg;
	}

	@JsonProperty("msg")
	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "AnyResPromptPayToUI [StatusCode=" + StatusCode + ", msg=" + msg + "]";
	}

}
