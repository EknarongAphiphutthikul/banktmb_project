import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import {Routes, RouterModule} from '@angular/router'
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import { StorageServiceModule} from 'angular-webstorage-service';

import { AppComponent } from './app.component';
import { PageLoginComponent } from './component/login/page-login/page-login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { ShowPromptpayComponent } from './component/promptpay/show-promptpay/show-promptpay.component';
import { ShowAccountComponent } from './component/account/show-account/show-account.component';
import { MainManuComponent } from './component/menu-nav/main-manu/main-manu.component';
import { TransferHomeComponent } from './component/transfer/transfer-home/transfer-home.component';
import { RestApiLocalBank } from './services/RestApiLocalBank';
import { NumberOnlyDirective } from './services/number.directive';

const router: Routes = [
  {path: 'login', component: PageLoginComponent},
  {path: 'account', component: ShowAccountComponent},
  {path: 'promptpay', component: ShowPromptpayComponent},
  {path: 'transfer', component: TransferHomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PageLoginComponent,
    ShowPromptpayComponent,
    ShowAccountComponent,
    MainManuComponent,
    TransferHomeComponent,
    NumberOnlyDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(router),
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    StorageServiceModule
  ],
  providers: [RestApiLocalBank],
  bootstrap: [AppComponent]
})
export class AppModule { }
