import { Headers, Http, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class RestApiLocalBank {
    constructor(private http: Http) {
    }
    private host = 'http://localhost:8090/TMB/v1';
    //method
    authentication(reqAuthentication) {
        return this.http.post(this.host + '/callRestApiLocalBank/authentication', reqAuthentication).toPromise();
    }

    getAccountsCus(id) {
        return this.http.get(this.host + '/callRestApiLocalBank/getAccounts/' + id).toPromise();
    }

    registerPromptPay(reqRegister) {
        return this.http.post(this.host + '/callRestApiInterBank/registerPromptpay', reqRegister).toPromise();
    }

    deletePromptPay(type, value) {
        return this.http.delete(this.host + '/callRestApiInterBank/deletePromptpay/' + type + '/' + value).toPromise();
    }

    transferOwnBank(reqtransfer) {
        return this.http.post(this.host + '/callRestApiLocalBank/transfer', reqtransfer).toPromise();
    }

    transferPromptPay(reqtransfer) {
        return this.http.post(this.host + '/callRestApiInterBank/TransferPromptpay', reqtransfer).toPromise();
    }

    GetAccountReveive(id) {
        return this.http.get(this.host + '/callRestApiLocalBank/getAccountReceive/' + id).toPromise();
    } 

    GetAccountReveivePromptPay(type, value) {
        return this.http.get(this.host + '/callRestApiInterBank/getPromptpay/' + type + '/' + value).toPromise();
    }
    
    GetStatement(req){
        return this.http.post(this.host + '/callRestApiLocalBank/showStatementTopFive', req).toPromise();
    }
}