import { Component, OnInit, Inject } from '@angular/core';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { RestApiLocalBank } from '../../../services/RestApiLocalBank';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transfer-home',
  templateUrl: './transfer-home.component.html',
  styleUrls: ['./transfer-home.component.css']
})
export class TransferHomeComponent implements OnInit {
  private check_typeTransfer: boolean;
  private check_selectAccount: boolean;
  private check_enterAccountReceive: boolean;
  private check_enterAmonut: boolean;
  private check_type_promptpay: boolean;
  private check_enterValuePromptPay: boolean;
  private value_accountReceive: string;
  private value_amonut: string;
  private value_promptpay: string;
  private listAccounts = [];
  private select_account: any;
  private check_ValidateInput: boolean;
  private value_nameAccountReceive: string;
  private alert_message:string;

  constructor(private router: Router,private callService: RestApiLocalBank, @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

  ngOnInit() {
    if (this.storage.get('statuslogin') == undefined) {
      this.router.navigate(['/login']);
    }
    this.check_typeTransfer = false;
    this.check_type_promptpay = false;
    this.check_selectAccount = false;
    this.check_enterAccountReceive = false;
    this.check_enterAmonut = false;
    this.check_enterValuePromptPay = false;
    this.check_ValidateInput = true;
    this.value_accountReceive = "";
    this.value_amonut = "";
    this.value_promptpay = "";
    this.select_account = '';
    this.alert_message = '';
    for (let entry of this.storage.get('listAccounts')) {
      this.listAccounts.push({ value: entry['AccountID'], viewValue: entry['AccountID'] })
    }
  }

  validateInput() {
    if (this.select_account != '') {
      this.check_selectAccount = false;
      if (this.check_typeTransfer) {
        // promptpay
        if (this.check_type_promptpay) {
          // mobile
          if (this.value_promptpay.length == 10) {
            this.check_enterValuePromptPay = false;
            this.check_ValidateInput = false;
          }
          else {
            this.check_enterValuePromptPay = true;
            this.check_ValidateInput = true;
          }
        }
        else {
          // id card
          if (this.value_promptpay.length == 13) {
            this.check_enterValuePromptPay = false;
            this.check_ValidateInput = false;
          }
          else {
            this.check_enterValuePromptPay = true;
            this.check_ValidateInput = true;
          }
        }
      }
      else {
        // own bank
        if (this.value_accountReceive.length == 14) {
          this.check_enterAccountReceive = false;
        }
        else {
          this.check_enterAccountReceive = true;
          this.check_ValidateInput = true;
        }
      }
    }
    else {
      this.check_selectAccount = true;
      this.check_ValidateInput = true;
    }
    if (this.value_amonut.length != 0) {
      this.check_enterAmonut = false;
      this.check_ValidateInput = false;
    }
    else {
      this.check_enterAmonut = true;
      this.check_ValidateInput = true;
    }
  }

  Event_BtnOKBankOwn() {
    this.validateInput();
    if (!this.check_ValidateInput) {
      this.callReveiveAccount(this.value_accountReceive);
    }
  }

  Event_BtnOKPromptPay() {
    this.validateInput();
    if (!this.check_ValidateInput) {
      let type_prompt = 'citizen';
      if (this.check_type_promptpay)
        type_prompt = 'mobile';
      this.callReveiveAccountPromptPay(type_prompt, this.value_promptpay);
    }
  }

  callReveiveAccount(accid) {
    let self = this;
    this.callService.GetAccountReveive(accid).then((response) => {
      const res = response.json();
      if (res['statusCode'] != 'ACT') {
        // alert("Account Receive Not Active");
        self.alert_message = 'Account Receive Not Active';
        document.getElementById("openModalAlert").click();
      }
      else {
        // get name account receive
        // console.log(res['name']);
        self.value_nameAccountReceive = res['name'];
        document.getElementById("openModalSameBank").click();
      }
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
      let error = JSON.parse(err['_body']);
      if (error['msg'] == 'Data Not Found') {
        // alert("Not Found Accound Receive");
        self.alert_message = 'Not Found Accound Receive';
        document.getElementById("openModalAlert").click();
      }
      else {
        // alert("Request Fail");
        self.alert_message = 'Request Fail';
        document.getElementById("openModalAlert").click();
      }
    });
  }

  callReveiveAccountPromptPay(type, value) {
    let self = this;
    this.callService.GetAccountReveivePromptPay(type, value).then((response) => {
      const res = response.json();
      if (res['Status'] != 'Active') {
        // alert("Account Receive Not Active");
        self.alert_message = 'Account Receive Not Active';
        document.getElementById("openModalAlert").click();
      }
      else {
        // get name account receive
        // console.log(res['AccountName']);
        self.value_nameAccountReceive = res['AccountName'];
        document.getElementById("openModalPromptpay").click();
      }
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
      let error = JSON.parse(err['_body']);
      if (error['msg'] == 'Account Not Found') {
        // alert("Citizen or Mobile Phone Not Found");
        self.alert_message = 'Citizen or Mobile Phone Not Found';
        document.getElementById("openModalAlert").click();
      }
      else {
        // alert("Request Fail");
        self.alert_message = 'Request Fail';
        document.getElementById("openModalAlert").click();
      }
    });
  }

  changeTypeTransfer() {
    this.check_enterAccountReceive = false;
    this.check_enterAmonut = false;
    this.check_enterValuePromptPay = false;
    this.check_selectAccount = false;
    this.check_ValidateInput = true;
    this.select_account = '';
    this.value_accountReceive = '';
    this.value_amonut = '';
    this.value_promptpay = '';
  }

  confirmPromptpay() {
    // console.log("confirmpromptpay");
    document.getElementById("closeModalPromptpay").click();
    let type_prompt = 'citizen';
    if (this.check_type_promptpay)
      type_prompt = 'mobile';
    this.callTransferPromptPay({
      "SendAccountID": this.select_account,
      "SendBankCode": "011",
      "SubmitAmount": this.value_amonut,
      "type": type_prompt,
      "value": this.value_promptpay
    });
  }

  confirmSameBank() {
    // console.log("confirmSameBank");
    document.getElementById("closeModalSameBank").click();
    this.callTransferSameBank(
      {
        "FeeAmount": 0,
        "ReceiveAccountID": this.value_accountReceive,
        "ReceiveBankCode": "011",
        "SendAccountID": this.select_account,
        "SendBankCode": "011",
        "SubmitAmount": this.value_amonut,
        "TxnNote": ""
      }
    );
  }

  callTransferPromptPay(req) {
    let self = this;
    this.callService.transferPromptPay(req).then((response) => {
      const res = response.json();
      // console.log(res);
      if (res['statusTransfer'] == true) {
        // alert("Transfer Success");
        self.alert_message = 'Transfer Success';
        document.getElementById("openModalAlert").click();
        self.changeTypeTransfer();
      }
      else {
        // alert("Transfer Fail");
        self.alert_message = 'Transfer Fail';
        document.getElementById("openModalAlert").click();
      }
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
      // alert("Request Fail");
      self.alert_message = 'Request Fail';
      document.getElementById("openModalAlert").click();
    });
  }

  callTransferSameBank(req) {
    let self = this;
    this.callService.transferOwnBank(req).then((response) => {
      const res = response.json();
      // console.log(res);
      if (res['statusTransfer'] == true) {
        // alert("Transfer Success");
        self.alert_message = 'Transfer Success';
        document.getElementById("openModalAlert").click();
        self.changeTypeTransfer();
      }
      else {
        // alert("Transfer Fail");
        self.alert_message = 'Transfer Fail';
        document.getElementById("openModalAlert").click();
      }
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
      // alert("Request Fail");
      self.alert_message = 'Request Fail';
      document.getElementById("openModalAlert").click();
    });
  }
}