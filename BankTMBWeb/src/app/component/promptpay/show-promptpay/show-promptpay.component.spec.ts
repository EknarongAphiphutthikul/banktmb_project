import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPromptpayComponent } from './show-promptpay.component';

describe('ShowPromptpayComponent', () => {
  let component: ShowPromptpayComponent;
  let fixture: ComponentFixture<ShowPromptpayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPromptpayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPromptpayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
