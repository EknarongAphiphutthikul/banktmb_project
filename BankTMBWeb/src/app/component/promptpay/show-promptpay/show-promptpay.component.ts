import { Component, OnInit, Inject } from '@angular/core';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { RestApiLocalBank } from '../../../services/RestApiLocalBank';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-promptpay',
  templateUrl: './show-promptpay.component.html',
  styleUrls: ['./show-promptpay.component.css']
})
export class ShowPromptpayComponent implements OnInit {
  private check_type: boolean;
  private check_Action: boolean;
  private check_selectAccount: boolean;
  private check_enterValue: boolean;
  private check_validteInput: boolean;
  private value_id: string;
  private listAccounts = [];
  private select_account: any;
  private alert_message:string;

  constructor(private router: Router, private callService: RestApiLocalBank, @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

  ngOnInit() {
    if (this.storage.get('statuslogin') == undefined) {
      this.router.navigate(['/login']);
    }
    this.check_type = false;
    this.check_Action = false;
    this.check_selectAccount = false;
    this.check_enterValue = false;
    this.check_validteInput = false;
    this.value_id = '';
    this.alert_message = '';
    for (let entry of this.storage.get('listAccounts')) {
      this.listAccounts.push({ value: entry['AccountID'], viewValue: entry['AccountID'] })
    }
  }

  findObjectAccount() {
    for (let entry of this.storage.get('listAccounts')) {
      if (entry['AccountID'] == this.select_account) {
        return {
          "AccountID": entry['AccountID'],
          "AccountName": this.storage.get('cusName'),
          "BankCode": entry['BankCode'],
          "IDType": '',
          "IDValue": this.value_id
        }
      }
    }
    return null;
  }

  validateInput() {
    if (this.check_type) {
      if (this.value_id.length == 10) {
        this.check_validteInput = false;
        this.check_enterValue = false;
      }
      else {
        this.check_validteInput = true;
        this.check_enterValue = true;
      }
    }
    else {
      if (this.value_id.length == 13) {
        this.check_validteInput = false;
        this.check_enterValue = false;
      }
      else {
        this.check_validteInput = true;
        this.check_enterValue = true;
      }
    }
    if (!this.check_Action)
      if (this.select_account != null) {
        this.check_selectAccount = false;
      }
      else {
        this.check_selectAccount = true;
        this.check_validteInput = true;
      }
  }

  changeAction() {
    this.value_id = '';
  }

  Event_BtnRegister() {
    this.validateInput()
    if (!this.check_validteInput) {
      let json_req = this.findObjectAccount();
      if (json_req != null) {
        if (this.check_type) {
          json_req['IDType'] = 'mobile';
        }
        else {
          json_req['IDType'] = 'citizen';
        }
        this.callPostRegister(json_req);
      }
      else {
        console.log("alert not complete");
      }
    };
  }

  Event_BtnRemove() {
    this.validateInput();
    if (!this.check_validteInput) {
      let type_prompt = 'citizen';
      if (this.check_type)
        type_prompt = 'mobile';
      this.callDeletePromptPay(type_prompt, this.value_id);
    }
  }

  callPostRegister(req) {
    let self = this;
    // console.log(req);
    this.callService.registerPromptPay(req).then((response) => {
      const res = response.json();
      // console.log(res);
      if (res['msg'] == 'success') {
        // alert("Register Success");
        self.callModalAlert("Register Success");
        this.changeAction();
      }
      else if (res['msg'] == 'exists') {
        // alert("Citizen or Mobile Phone Duplicate");
        self.callModalAlert("Citizen or Mobile Phone Duplicate");
      }
      else {
        // alert("Register Fail");
        self.callModalAlert("Register Fail");
      }
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  callDeletePromptPay(type: any, value: any) {
    let self = this;
    // console.log('para delete prompypay' + type + value);
    this.callService.deletePromptPay(type, value).then((response) => {
      const res = response.json();
      // console.log(res);
      if (res['msg'] == 'success') {
        // alert("Remove Success");
        self.callModalAlert("Remove Success");
        this.changeAction();
      }
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
      let error = JSON.parse(err['_body']);
      if(error['msg']== 'Account Not Found'){
        // alert("Citizen or Mobile Phone Not Registered");
        self.callModalAlert("Citizen or Mobile Phone Not Registered");
      }
      else{
        // alert("Request Fail");
        self.callModalAlert("Request Fail");
      }
    });
  }

  callModalAlert(msg:string){
    this.alert_message = msg;
    document.getElementById("openModalAlert").click();
  }
}

