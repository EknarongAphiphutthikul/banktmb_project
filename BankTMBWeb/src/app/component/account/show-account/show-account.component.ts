import { Component, OnInit, Inject } from '@angular/core';
import { RestApiLocalBank } from '../../../services/RestApiLocalBank';
import { Response } from '@angular/http'
import { HttpHeaderResponse, HttpErrorResponse } from '@angular/common/http';
import { WebStorageService, LOCAL_STORAGE } from 'angular-webstorage-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show-account',
  templateUrl: './show-account.component.html',
  styleUrls: ['./show-account.component.css']
})
export class ShowAccountComponent implements OnInit {

  constructor(private router: Router,private callService: RestApiLocalBank, @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }
  private list_account:any;
  private list_statement:any;
  private select_account:string;

  ngOnInit() {
    if (this.storage.get('statuslogin') == undefined) {
      this.router.navigate(['/login']);
    }
    this.callGetAccount();
  }

  callGetAccount() {
    let self = this;
    this.callService.getAccountsCus(this.storage.get('cusID')).then( (response) => {
      const res = response.json();
      this.storage.set('listAccounts', res); 
      self.list_account = res;
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
    });
  }

  show_Statement(index){
    this.select_account = index;
    this.callGetStatement({
      "AccountID": index,
      "End": "",
      "Start": "",
      "Type": ""
    });
  }

  callGetStatement(req) {
    // console.log(req);
    let self = this;
    this.callService.GetStatement(req).then( (response) => {
      const res = response.json();
      // console.log(res);
      self.list_statement = res;
      document.getElementById("openShowStatement").click();
    }).catch((err: HttpErrorResponse) => {
      console.log(err);
      self.list_statement = []
      document.getElementById("openShowStatement").click();
    });
  }
}
