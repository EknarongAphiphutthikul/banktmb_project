import { Component, OnInit, Inject } from '@angular/core';
import { WebStorageService, LOCAL_STORAGE, SESSION_STORAGE } from 'angular-webstorage-service';
import { RestApiLocalBank } from '../../../services/RestApiLocalBank';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-manu',
  templateUrl: './main-manu.component.html',
  styleUrls: ['./main-manu.component.css']
})
export class MainManuComponent implements OnInit {

  constructor(private router: Router, private callService: RestApiLocalBank, @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

  ngOnInit() {
    if (this.storage.get('statuslogin') == undefined) {
      this.router.navigate(['/login']);
    }
  }

  account() {
    this.router.navigate(['/account']);
  }

  Transfer() {
    this.router.navigate(['/transfer']);
  }

  PrompPay() {
    this.router.navigate(['/promptpay']);
  }

  SignOut() {
    window.localStorage.clear();
    this.router.navigate(['/login']);
  }

}
