import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http'
import { RestApiLocalBank } from '../../../services/RestApiLocalBank';
import { HttpErrorResponse } from '@angular/common/http';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.css']
})
export class PageLoginComponent implements OnInit {

  private inputEmail: string;
  private inputPassword: string;
  private checkAlertEmailPassword: boolean;
  private reqAuthentication = {
    "password": '',
    "username": ''
  }

  constructor(private router: Router, private callService: RestApiLocalBank, @Inject(LOCAL_STORAGE) private storage: WebStorageService) { }

  ngOnInit() {
    this.inputEmail = '';
    this.inputPassword = '';
    this.checkAlertEmailPassword = false;
  }

  validate_input() {
    if (this.inputEmail.length == 0 || this.inputPassword.length == 0) {
      this.checkAlertEmailPassword = true;
    }
    else{
      this.checkAlertEmailPassword = false;
    }
  }

  Event_BtnLogin() {
    this.validate_input()
    if(!this.checkAlertEmailPassword){
      this.reqAuthentication.username = this.inputEmail;
      this.reqAuthentication.password = this.inputPassword;
      this.callAuthentication();
    }
  }

  callAuthentication() {
    this.callService.authentication(this.reqAuthentication).then(
      (response: Response) => {
        let data = response.json();
        if(data["statusAuthentication"] == "PasswordCorrect"){
          this.storage.set('statuslogin', true);
          this.storage.set('cusName', data['Name'])
          this.storage.set('cusID', data['CustomerID'])
          this.router.navigate(['/account']);
        }
        else{
          this.checkAlertEmailPassword = true;
        }
      }).catch((err: HttpErrorResponse) => {
        console.log(err);
    });
  }
}
